import sys
sys.path.append('/Users/mikeh/home/projects/codeStories/')
from codestories.cs.tools.tracer import MagicTrace
from codestories.cs.CodeStories import CodeStory

def for_loop_fun():
    numbers = [1, 2, 3, 4, 5, 6]
    for n in numbers:
        print(n, '/', 2, '=', n/2)
        print(n, '//', 2, '=', n//2)
        print(n, '%', 2, '=', n%2)

    return n

def while_loop_example():
    count = 0
    while count < 5:
        print(count)
        count = count + 1
    return count

def while_loop_fun():
    count = 0
    while count%4 != 3:
        print(count)
        print(count % 4)
        count = count + 1

    # return count

def loops0():
    data = MagicTrace.trace_function(while_loop_example, params=[])
    raw_html = MagicTrace.generate_files(data, as_one=True)
    with open('loop0.html', 'w') as fd:
        fd.write(raw_html)
        print('wrote loop0.html')

def loops1():
    data = MagicTrace.trace_function(for_loop_fun, params=[])
    raw_html = MagicTrace.generate_files(data, as_one=True)
    with open('loop1.html', 'w') as fd:
        fd.write(raw_html)
        print('wrote loop1.html')

def loops2():
    data = MagicTrace.trace_function(while_loop_fun, params=[], debug=True)
    raw_html = MagicTrace.generate_files(data, as_one=True)
    with open('loop2.html', 'w') as fd:
        fd.write(raw_html)
        print('wrote loop2.html')

def is_even(n):
    print('HERE I AM')
    return n % 2 == 0

def all_even():
    count = 0
    for i in range(0, 20):
        if not is_even(i):
            print('odd')
            count += 1
            break
    print('done with loop')
    return 4

def context():

    from codestories.cs.tools.tracer import MagicTrace
    # MagicTrace.play_movie(all_even, context=[is_even])
    data = MagicTrace.trace_function(all_even, context=[is_even], debug=False)
    raw_html = MagicTrace.generate_files(data, as_one=True)
    with open('context.html', 'w') as fd:
        fd.write(raw_html)

#loops1()
#loops2()
context()
