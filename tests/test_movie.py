

# conda install -c conda-forge pysnooper
# pip install too

def simple_loop(a):
    k = -1
    apples={'name', 'sweet'}
    for i in range(0, 3):
        if i < a:
            print(i, 'yes')
            # continue
        else:
            print(i, 'no')
            #is_one()
            return
        a = a - 1

    return k

def test_it():
    code = '''\
a = 10
def is_one():
    return True

def simple_loop(a):
    k = -1
    apples={'name', 'sweet'}
    for i in range(0, 3):
        if i < a:
            print(i, 'yes')
            # continue
        else:
            print(i, 'no')
            #is_one()
            return
        a = a - 1

    return k
'''

# data = MagicTrace.play_code(code, 'simple_loop', params=[1])

def print_dog_poem():
    print("I LIKE")
    print("Dogs")
    print("Black Dogs, Green Dogs")
    print("Bad Dogs, Mean Dogs")
    print("All kinds of Dogs")
    print("I like Dogs")
    print("Dogs")

poem="""
def print_dog_poem():
    print("I LIKE")
    print("Dogs")
    print("Black Dogs, Green Dogs")
    print("Bad Dogs, Mean Dogs")
    print("All kinds of Dogs")
    print("I like Dogs")
    print("Dogs")
"""

def words():
  w = "ab c Ie".split()
  for idx, n in enumerate(w):
    print(idx, n)
    if n.islower():
      print('is lower', n)

import sys
sys.path.append('/Users/mikeh/home/projects/codeStories/')
#sys.path.append('..')


from codestories.cs.tools.tracer import MagicTrace
from codestories.cs.CodeStories import CodeStory

def test_two():
    data = MagicTrace.trace_function(simple_loop, params=[1])
    files = MagicTrace.generate_files(data, as_one=True)
    print('test_two', files)

#test_two()
#import sys
#sys.exit(1)

def test_poem():
    html = MagicTrace.play_movie(print_dog_poem)
    with open('poem.html', 'w') as fd:
        fd.write(html)
        print('wrote poem.html')

    html = MagicTrace.play_movie(simple_loop, params=[1])
    with open('simple_loop2.html', 'w') as fd:
        fd.write(html)
        print('wrote simple_loop2.html')

    # poem has a call already
    data = MagicTrace.trace_code(poem, fn_name='print_dog_poem')
    raw_html = MagicTrace.generate_files(data, as_one=True)
    with open('poem2.html', 'w') as fd:
        fd.write(raw_html)
        print('wrote poem2.html')

test_poem()
import sys
sys.exit(1)

c = """\
def is_even(n):
  return n%2 == 0
  
def not_used(x):
   if x == 2:
      pass
   return
   
def all_even():
  count = 0
  for i in range(0, 20):
    if is_even(i):
      print("even\\n",i)
      count +=1
      
ide.magic()
"""

c2 = """\
def simple():
    for i in range(0, 4):
    
        if i > 2:
            print('more')
        elif i == 2:
            print('equal')
        else:
            print('less')
"""

#data = MagicTrace.trace_code(c, 'all_even')
data = MagicTrace.trace_code(c2, 'simple')
files = MagicTrace.generate_files(data, as_one=False)
print(files)

# data = MagicTrace.trace_function(words)
# raw_html = MagicTrace.generate_files(data, as_one=True)
# with open('out3.html', 'w') as fd:
#     fd.write(raw_html)
#     print('wrote out3.html')


'''
uncss 'http://localhost:3000/code' > out.css
uncss --ignore /.tcode/,/.memory/,/.trace/,/before/ 'http://localhost:3000/code' > out.css
cleancss -o magic.min.css out.css
cp magic.css ../codestories/cs/tools/tracer
'''