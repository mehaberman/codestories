from unittest.mock import Mock

class MyMock(Mock):

    def __repr__(self):
        return '';

def install_ide(*args):
    class IDE():
        #tester = Nop("")
        #reader = Nop("")
        #tester = IDE() 
        #reader = IDE()
        def nop(self, *args, **kw): 
            return self.nop
        def __getattr__(self, _):
            print('WHAT',_)
            return self.nop
    return IDE()

ide = install_ide()
ide = MyMock()
ide.welcome()
print('tester', ide.tester.test_function())
print('IDE', ide.welcome())
print('IDE', ide.welcome().what)
print('IDE', ide.welcome('hello').what())
