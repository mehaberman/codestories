from enum import Enum

class Level(Enum):
    RAW = 0
    REMOVE_MAGIC = 1
    REPLACE_IDE = 2
    AS_IS = 3
    IPY_REMOVE = 5
    SCOPE0_NON_PRINT = 10 
    SCOPE0 = 20
    SCOPE0_ANY_PRINT = 30
    DEFAULT = 30
    
    @staticmethod
    def get_level(v_str):
        if v_str == 'as_is':
            return Level.AS_IS
        if v_str == 'keep_scope0_print':
            return Level.SCOPE0_NON_PRINT
        if v_str == 'keep_scope1_print':
            return Level.SCOPE0
        return Level.DEFAULT

level = Level.get_level('as_is')
print(level)
print(level.value <= Level.DEFAULT.value)
