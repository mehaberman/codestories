import pysnooper

@pysnooper.snoop(depth=1)
def simple_loop(a):
    k = -1
    apples={'name', 'sweet'}
    for i in range(0, 3):
        if i < a:
            print(i, 'yes')
            # continue
        else:
            print(i, 'no')
            return
        a = a - 1

    return k

#@pysnooper.snoop(depth=1)
def do_one(k):
    if k > 10:
        return 4
    return 2

@pysnooper.snoop(depth=1)
def do_two():
    for i in range(0,5):
        a = do_one(i+4)
        print('hello',i, a)
        #do_one(a)
    return i

@pysnooper.snoop(depth=1)
def words():
  w = "ab c Ie".split()
  for idx, n in enumerate(w):
    print(idx, n)
    if n.islower():
      print('is lower',n)

#words()
#simple_loop(1)
do_two()