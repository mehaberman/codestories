import sys
sys.path.append('..')
from codestories.cs import CodeStories
from codestories.cs.ide.colab.Colab import ColabNotebookInstaller, ColabFramework
from codestories.cs.ide import Notebook
from codestories.cs.parsers import CodeCleaners
from codestories.cs.ide.Client import ClientAPI

sandbox = CodeStories.sandbox

def test_simple(tag):
    t1 = CodeStories.CodeStory()
    t2 = CodeStories.CodeStory('mikeh', lesson_tag=tag)
    print(t2.welcome())

def test_framework(tag=None, filename=None):
    meta = sandbox.get_meta()
    meta.lesson_tag = tag
    meta.notebook_file = filename
    meta.u_id = 'mikeh'
    fr = ColabFramework()
    client = ClientAPI()
    fr.pre_install(meta, client)
    tester = fr.get_tester()
    print(fr)
    print(tester)

    tester._generate_code_file()

def test_parser(filename):
    with open(filename, 'r') as fd:
        nb = Notebook.StoryNotebook(fd.read())
        cleaner = CodeCleaners.CodeCleaner()
        code = cleaner.clean(nb)
        print('CODE')
        print(code)

def test_fetch_once():
    # watch out for too many network requests
    NB = '1GBamwb67eCSPp0YreXMIrFF-IZOT1nT3'
    nb = ColabNotebookInstaller(NB)
    nb.read_ipynb(file_out='./sandbox_tmp/solution.json')

def simple_client(tag):

    # set up the sandbox and env.
    tag = 'dmap:mls:gradient_descent'
    server = 'http://127.0.0.1:8080'
    cs = CodeStories.CodeStory(lesson_tag=tag, server_id=server)
    framework = cs.framework
    client = framework.client
    tests = client.get_tests()
    print(tests)

def test_reader(tag,filename):
    #from codestories.cs.readers.Readers import AssetReader
    #ar = AssetReader('dmap:projects:project-m3', Frameworks.ASSET_DIR)
    cs = CodeStories.CodeStory(lesson_tag=tag, notebook_file=filename)
    reader = cs.reader


tag = 'dmap:projects:project-m3'
ipy = './sandbox_tmp/project-m3.ipynb'

tag = 'p4ds:bootcamp:variables'
ipy = './sandbox_tmp/variables.ipynb'
#test_parser(ipy)

#test_simple()
test_framework(tag, ipy)
# test_reader(tag, ipy)

#test_fetch_once()
#simple_client()
#import astunparse
#print(astunparse.__version__)
