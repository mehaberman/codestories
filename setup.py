import setuptools

# see https://packaging.python.org/tutorials/packaging-projects/


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="codestories-pkg-mh",
    version="1.0.1",
    author="Michael Haberman",
    author_email="author@example.com",
    description="A code for stories",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://mehaberman@bitbucket.org/mehaberman/codestories",
    packages=setuptools.find_packages(),
    package_data={
        'codestories.cs.tools.tracer': ['*.js', '*.css'],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # astunparse 1.6.3 is installed on colab
    # but it stills tries to install it
    # and it causes conflicts with nbclient

    install_requires=[
       # "astunparse >= 1.6.3",  # better dictionary parsing
       # "astunparse",  # better dictionary parsing
    ],
    python_requires='>=3.6',
)