import os
import sys

'''
from dmap.lessons.tfidf.lib import Util
text = Util.read_local_file('./info490/assets/src/dmap/lessons/tfidf/data/cith.txt')
OR
use the reader !!!
'''




def in_path(dir):
    for p in sys.path:
        if p.find("/src") >= 0:
            return True
    return False


class AssetReader(object):

    def __init__(self, lesson_tag, asset_src, root=None, debug=False):

        self.debug = debug
        parts = lesson_tag.lower().split(':')
        course    = parts[0]  # dmap
        classroom = parts[1]  # projects
        lesson    = parts[2]  # project-m3

        base_path = "{:s}/{:s}".format(course, classroom)

        # remote reading
        self.url = root
        if debug:
            print('remote URL Assets', self.url)

        # if root is not None:
        #     self.url = "{git:s}/{base:s}/{lesson:s}".format(git=root, base=base_path, lesson=lesson)
        #     if debug:
        #         print('remote URL Assets', self.url)

        # root for remote fetching
        # set on install: /content/info490/assets
        # fq = os.path.abspath(os.path.dirname(__file__))
        # asset_dir = os.path.dirname(fq)
        # assert asset_dir.find('/src') > 0, "bad asset path " + asset_dir

        asset_dir = os.path.abspath(asset_src)
        if debug:
            print('install path', asset_dir)

        if asset_dir not in sys.path:
            sys.path.append(asset_dir)

        # TODO: fix the tarball so 'lessons' isn't written
        base_dir = "{:s}/lessons/{:s}".format(asset_dir, base_path)
        self.lesson_base = "{:s}/{:s}".format(base_dir, lesson)
        if debug:
            print('File Assets', self.lesson_base)

        # allow imports of lib
        # these files MUST be available on test framework too
        # saw a one time issue that without the trailing slash, import LessonUtil did NOT work
        lib_dir = "{:s}/lib/".format(self.lesson_base)
        if lib_dir not in sys.path:
            sys.path.append(lib_dir)

        try:
            from IPython.display import display, clear_output
            self.player = display
        except ImportError:
            print("IPython not installed: unable to display html")
            self.player = None

    def read_local(self, filename):
        fn = "{:s}/{:s}".format(self.lesson_base, filename)
        with open(fn, 'r') as fd:
            return fd.read()

    def view_section(self, section, remote=False):

        section = int(section)
        text = 'n/a'

        if remote:
            if self.url is None:
                text = "remote asset url unavailable"
            else:
                url = "{:s}/html/section{section:02d}.html".format(self.url, section=section)
                try:
                    import requests
                    r = requests.get(url)
                    r.encoding = 'utf-8'
                    if r.status_code == requests.codes.ok:
                        text = r.text
                    else:
                        text = "Error on fetch:" + str(r.status_code)
                        text += "\n" + url
                except Exception as e:
                    text = "Unable to get" + url
                    text += "\n" + str(e)
        else:
            try:
                fq_path = "html/section{:02d}.html".format(section)
                text = self.read_local(fq_path)
            except FileNotFoundError:
                text = "File Not Found: " + self.lesson_base + "/" + fq_path

        if self.player:
            import IPython
            from IPython.display import display, clear_output
            #print('nb', len(text))
            display(IPython.display.HTML(text))
        else:
            #print('txt', len(text))
            print(text)

        # avoid printing anything out
        # way to suppress output on the return ??
        return None
