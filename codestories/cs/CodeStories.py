

import codestories.cs.SandBox as SandBox
sandbox = SandBox.SandBox()
logger = sandbox.get_logger()

from .ide.Frameworks import DefaultFramework, StaticFileAPI, ASSET_DIR
from .ide.colab.Colab import ColabFramework

from codestories.cs.readers.Readers import AssetReader

class CodeStory(object):

    def __init__(self, user_id=None, lesson_tag=None, notebook_id=None, server_id=None, notebook_file=None):

        # could force a re-birth of meta data if you want to lose cached values
        self.meta = sandbox.get_meta()

        self.meta.update(user_id=user_id)
        self.meta.update(lesson_tag=lesson_tag)
        self.meta.update(notebook_id=notebook_id)
        self.meta.update(notebook_file=notebook_file)

        self.framework = DefaultFramework(self.meta, server_id)

        api = None
        try:
            from google.colab import _message
            api = ColabFramework()
            logger.log('using ColabFramework')
        except (ImportError, ModuleNotFoundError) as e:
            if notebook_id is not None:
                api = StaticFileAPI()
                logger.log('using static file DefaultFramework')
        except:
            logger.log('exception on install')
            pass

        if api is None:
            from unittest.mock import Mock
            print('using mock Framework')
            self.framework = Mock()
            self._reader = Mock()
            self.api = Mock()
        else:
            # downloads the notebook and reads the notebook meta data
            self.framework.install_api(self.meta, api)
            self._reader = AssetReader(lesson_tag, ASSET_DIR, root=self.meta.root)
            self.api = api

    def __repr__(self):
        return str(self.framework)

    @property
    def tester(self):
        return self.framework.get_tester()

    @property
    def reader(self):
        return self._reader

    def welcome(self):
        # name = self.meta.notebook_name
        name = self.meta.lesson_name
        return 'Telling the story: ' + name

    def is_valid(self):
        return self.api.is_valid(self.meta)

    def play_movie(self, fn):
        from codestories.cs import MagicTrace
        return MagicTrace.play_movie(fn)



    def _debug(self):
        return 'Framework' + str(self.framework) + '\n' + str(self.meta)
