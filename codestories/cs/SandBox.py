import os
import time
from datetime import datetime

from codestories.cs.tools.Utilities import singleton, SimpleLogger

SANDBOX_DIR = "sandbox_tmp"
SESSION_FILE = "{:s}/session.txt".format(SANDBOX_DIR)


class MetaData(object):

    def __init__(self, lesson_tag=None, notebook_id=None):

        self.raw  = {}

        # todo rename: to git_raw_root_repo
        self.root = None

        self.lesson_tag   = lesson_tag # dmap:projects:project-m3
        self.lesson_name  = None   # static, title of lesson
        self.course       = None  # dmap
        self.classroom = None     # projects
        self.lesson    = None     # project-m3

        self.notebook_id = notebook_id
        self.notebook_name = None # will change if user changes notebook name
        self.notebook_file = None # if using static content, server side testing

        self.user_id = None # assigned by user
        # unique string to identify client(colab, jupyter) + user
        self.c_id = None
        # colab it's just the notebook id

        # what's inside the notebook (can change as the notebook is updated, user logs in)
        self.u_id   = '0'
        self.u_name = 'na'
        self.min_time = 0
        self.max_time = 0

    def update(self, raw=None, root=None, lesson_tag=None, lesson_name=None,
               notebook_id=None, notebook_name=None, notebook_file=None,
               min_time=0, max_time=0, user=None, user_id=None, c_id=None):

        if raw is not None:
            self.raw = raw

        if root is not None:
            self.root = root

        if lesson_tag is not None:
            self.lesson_tag = lesson_tag
            parts = lesson_tag.split(':')
            self.course = parts[0]
            self.classroom = parts[1]
            self.lesson = parts[2]

        if lesson_name is not None:
            self.lesson_name = lesson_name

        if notebook_id is not None:
            self.notebook_id = notebook_id

        if user_id is not None:
            self.user_id = user_id

        if c_id is not None:
            self.c_id = c_id

        if notebook_file is not None:
            self.notebook_file = notebook_file

        if notebook_name is not None:
            self.notebook_name = notebook_name

        if self.min_time == 0 or min_time < self.min_time:
            self.min_time = min_time

        if self.max_time == 0 or max_time > self.max_time:
            self.max_time = max_time

        # TODO: push this up to the specific framework
        # for colab notebooks
        # user_id is set by the USER
        # u_id is set by colab, no tampering
        if user is not None:
            self.u_id   = user.get('id', self.u_id)
            self.u_name = user.get('name', self.u_name)


    def __repr__(self):
        kv = self.kv()

        kv['notebook_file'] = self.notebook_file
        kv['notebook_name'] = self.notebook_name
        kv['lesson_name'] = self.lesson_name
        kv['c_id'] = self.c_id

        return str(kv)

    def kv(self):

        result = {
            'lesson_tag': self.lesson_tag,
            'notebook_id': self.notebook_id,
            'c_id': self.c_id,
            'user_id': self.user_id,
            'u_id': self.u_id,
            'u_name': self.u_name,
            'min_time': self.min_time,
            'max_time': self.max_time,
        }

        return result

@singleton
class SandBox(object):

    def __init__(self):
        if not os.path.isdir(SANDBOX_DIR):
            os.mkdir(SANDBOX_DIR)
        self._logger = SimpleLogger(SANDBOX_DIR)
        self._sandbox = SANDBOX_DIR

        self._meta = MetaData()

        # a session has to do with persistence
        # on line editors (colab, replit) will timeout after
        # non activity
        self.session_created = False
        if not os.path.exists(SESSION_FILE):
            self.session_created = True
            with open(SESSION_FILE, 'w') as fd:
                now = time.time()
                dt = datetime.fromtimestamp(now)
                fd.write(dt.strftime('%Y-%m-%d %H:%M:%S'))
                fd.close()

        #print(os.getcwd())
        #print('SANDBOX init', SESSION_FILE, self.session_created, os.path.exists(SESSION_FILE))

    def get_logger(self):
        return self._logger

    def get_meta(self):
        return self._meta

    def get_sandbox_dir(self):
        return self._sandbox

    def __repr__(self):
        return self.get_sandbox_dir()

    def get_session_information(self):
        #print(os.getcwd())
        #print('SANDBOX info', SESSION_FILE, self.session_created, os.path.exists(SESSION_FILE))
        s_time = os.path.getmtime(SESSION_FILE)  # modified time
        return self.session_created, s_time
