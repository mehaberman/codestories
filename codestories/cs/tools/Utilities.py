from datetime import datetime
import time
import os

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

def is_ipython(text):
    return text is not None and text.find('{"nbformat') == 0

def timestamp_to_str(t):
    dt = datetime.fromtimestamp(t)
    r = dt.strftime('%Y-%m-%d %H:%M:%S')
    return r

def read_cache(filename, cache_time=1, logger=None):

    n_time = time.time()
    if os.path.exists(filename):
        do_read_cache = False
        m_time = os.path.getmtime(filename)  # modified time
        dt0 = datetime.fromtimestamp(m_time)
        dt1 = datetime.fromtimestamp(n_time)
        age = (dt1 - dt0).total_seconds()

        if age < cache_time:
            do_read_cache = True

        if do_read_cache:
            if os.path.getsize(filename) > 0:
                if logger is not None:
                    logger.log("Reading cached version:", filename, ":")
                with open(filename, 'r') as fd:
                    return fd.read(), m_time, True
            else:
                if logger is not None:
                    logger.log("Unable to read cache: Bad file size")

    return None, None, None

class SimpleLogger(object):

    id_ = 0

    def __init__(self, path='.'):
        self.logger = open("{:s}/debug_log{:d}.txt".format(path, SimpleLogger.id_), "w")
        SimpleLogger.id_ += 1

    def log(self, *args):
        to_write = " ".join([str(a) for a in args])
        self.logger.write(to_write + '\n')
        self.logger.flush()