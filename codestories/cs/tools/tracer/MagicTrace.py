
'''
# from dill.source import getsource
# import dis
# print(dis.dis(do_one))
# print(getsource(do_one))
'''

import os
import sys
import re

from codestories.cs.SandBox import SandBox
sandbox = SandBox()
logger = sandbox.get_logger()

def parse_tracer_code(result, line_offset, max_lines):

    exec_info = []
    memory_info = []
    return_line = 0

    explicit_return = False
    for line in result.split('\n'):
        # print('io', line)
        clean = line.strip()
        if len(clean) == 0:
            continue

        parts = re.split(r'\s+', clean)
        command = parts[0]
        if command == 'call' or command == 'line':
            lno = parts[1]
            token = ' {:} '.format(lno)
            idx = line.index(token)
            code = line[idx + len(token):]
            lno = int(lno) - line_offset
            # info = {'run': {'lno': int(lno) - line_offset, 'code': code}}
            info = {'type': 'run', 'value': {'name': 'lno', 'value': lno}}
            exec_info.append(info)
        else:
            if command == 'Modified' or command == 'New' or command == 'Starting':
                var = parts[2]  # i = ....
                idx = line.index('=')
                value = line[idx+1:].strip()
                # value could have a '#' after it  i = 0 # starting value
                # info = {'memory': {'name': var, 'value': value}}
                info = {'type': 'memory', 'value': {'name': var, 'value': value}}
                exec_info.append(info)
                if var not in memory_info:
                    memory_info.append(var)

            elif command == 'Return':
                if not explicit_return:
                    # on an implicit return, go to the next line
                    info = {'type': 'run', 'value': {'name': 'lno', 'value': return_line}}
                    exec_info.append(info)

                value = parts[2]
                # info = {'memory': {'name': 'return', 'value': value}}
                info = {'type': 'memory', 'value': {'name': 'return', 'value': value}}
                exec_info.append(info)
                memory_info.append('return')

            elif command == 'return':
                # return      30     return i
                # info = {'memory': {'name': 'return', 'value': value}}
                # actual return value is not given here
                # code has an explicit return statement in it if the rhs also contains 'return'
                lno = parts[1]
                lno = int(lno) - line_offset
                code = ' '.join(parts[2:])
                if code.find('return') >= 0:
                    explicit_return = True
                    return_line = lno
                    # print('EXPLICIT RETURN')
                else:
                    # print('IMPLICIT RETURN', lno)
                    return_line = max_lines

                info = {'type': 'run', 'value': {'name': 'lno', 'value': lno}}
                exec_info.append(info)


            elif command == 'Elapsed' or command == 'Source':
                pass
            else:
                # only work if normalize=False
                #import re
                #if re.match(r'^[0-9]+:[0-9]+:[0-9]+', command):
                #    # TODO: if > 100 ms don't play the movie
                #    # or if output is too long
                #    pass
                # output.append(command.strip())

                var = 'stdout'
                value = line.strip()
                try:
                    last = exec_info[-1]
                except IndexError:
                    print("Bad Index", result)
                    continue

                try:
                    info = last['stdout']
                    #print('LAST', info, value)
                    if info['name'] == var:
                        info = exec_info.pop()['stdout']
                        value = info['value'] + ' ' + value
                except KeyError:
                    pass

                # info = {'stdout': {'name': var, 'value': value}}
                info = {'type': 'output', 'value': {'name': 'stdout', 'value': value}}
                exec_info.append(info)

    return exec_info, memory_info

def build_html_code(data):
    rows = data['code']
    import html
    code = []
    for idx, line in enumerate(rows):
        lno = idx + 1
        line = html.escape(line)
        line = line.replace('{', '&#123;')
        line = line.replace('}', '&#125;')
        if len(line.strip()) == 0:
            line = '&nbsp;'
        code.append(line)
    return '\n'.join(code)

def build_html(data):
    body = ''
    rows = data['code']
    import html
    for idx, line in enumerate(rows):
        lno = idx+1
        line = html.escape(line)
        line = line.replace('{', '&#123;')
        line = line.replace('}', '&#125;')
        if len(line.strip()) == 0:
            line = '&nbsp;'
        code = '<pre>{:s}</pre>'.format(line)
        code = '<div class="tcontent">{:s}</div>'.format(code)
        body += '\n<div class="tcode" id="lno_{:d}">{:s}</div>'.format(lno, code)

    items = []
    for var in data['memory']:
        memory = '<pre class="variable">{:s}</pre>'.format(var)
        memory += '<pre class="value">{:s}</pre>'.format('&nbsp;')
        row = '<div id="var_{:s}" class="row">{:s}</div>'.format(var, memory)
        items.append(row)

    var = 'stdout'
    memory = '<pre class="variable">{:s}</pre>'.format(var)
    memory += '<pre class="value">{:s}</pre>'.format('&nbsp;')
    std_row = '<div id="var_{:s}" class="row">{:s}</div>'.format(var, memory)

    title1 = '<div>Memory</div>'
    rows = '\n'.join(items)
    title2 = '<div>Output</div>'

    code = '<div class="trace container">{:s}\n</div>'.format(body)
    mem = '<div class="memory container">{:s}\n{:s}<br/>{:s}{:s}</div>'.format(title1, rows, title2, std_row)

    return '<div class="code-movie">\n{:s}\n{:s}</div>'.format(code, mem)


def build_call(fn_name, params):
    buffer = ''
    buffer += "\n{:s}(".format(fn_name)
    for idx, k in enumerate(params):
        if isinstance(k, str):
            p = '"{:s}"'.format(k)
        else:
            p = str(k)
        buffer += p
        if idx+1 < len(params):
            buffer += ','
    buffer += ")\n"
    return buffer


def trace_code(code, fn_name, params=[], debug=False):

    # using unique filenames
    # prevents issues with dynamic mod loading on the same name
    import uuid
    import importlib
    import inspect

    assert fn_name is not None, 'invalid function '

    i_dir = sandbox.get_sandbox_dir()

    if i_dir not in sys.path:
        sys.path.append("{:s}/".format(i_dir))

    u_id = uuid.uuid4()
    TMP_MOD   = 't_'+str(u_id)
    MOVIE_MOD = 'm_'+str(u_id)

    TMP_FILE = "{:s}/{:s}.py".format(i_dir, TMP_MOD)
    MOVIE_FILE = "{:s}/{:s}.py".format(i_dir, MOVIE_MOD)

    # only want functions
    # if code is clean, could just write it out!
    with open(TMP_FILE, 'w') as fd:
        for line in code.split('\n'):
            if len(line) > 0:
                first = line[0]
                if str.isspace(first) or line.startswith('def'):
                    fd.write(line)
                    fd.write('\n')
            else:
                fd.write('\n')
        # fd.write('\n')

    # functions_list = re.findall(r'^def\s+([a-z_][A-Za-z_]+)', code, flags=re.M)
    # this has to be a safe load, no calling of functions
    tmp = importlib.import_module(TMP_MOD)
    functions_list = [o for o in inspect.getmembers(tmp) if inspect.isfunction(o[1])]
    # raw_code = inspect.getsource(tmp)

    rest = ''
    first = ''
    for name, fn in functions_list:
        c = inspect.getsource(fn)
        if name == fn_name:
            first = c
        else:
            rest += c
    line_count = len(first.split('\n'))
    raw_code = first + '\n' + rest
    raw_code = raw_code.strip() + '\n\n'  # add one extra line at the end of the file
    header = 'import pysnooper\n@pysnooper.snoop(normalize=True)\n'
    code = header + raw_code
    # we added two lines of code
    line_offset = 2

    with open(MOVIE_FILE, 'w') as fd:
        fd.write(code)

    from unittest.mock import patch
    import io

    # from contextlib import redirect_stdout
    # f = io.StringIO()
    # with redirect_stderr(f):
    #    import movie
    # result = f.getvalue().split('\n')

    # now run the module with the tracer code
    # note call the function from here
    # alternative, is to add a function call to the module
    # see above
    one_stream = io.StringIO()
    with patch('sys.stderr', new=one_stream) as fake_io:
        with patch('sys.stdout', new=one_stream) as fake_stdout:
            # when the module is loaded, we want NO code to run
            mod = importlib.import_module(MOVIE_MOD)
            if fn_name is not None:
                fn = getattr(mod, fn_name)
                # now call the entry point
                # to begin the trace
                fn(*params)
            else:
                print('SKIP CALL')

    #output = fake_stdout.getvalue()
    #print(output)

    result = fake_io.getvalue()
    code = raw_code.split('\n')
    exec_info, memory_info = parse_tracer_code(result, line_offset, line_count)

    if debug:
        print(result)

    # the module that is traced does NOT have a call to the function
    # add it here to show what happened
    # add the function call as a string at the end
    fn_call = build_call(fn_name, params)
    code.append(fn_call)
    info = {'type': 'run', 'value': {'name': 'lno', 'value': len(code)}}
    exec_info.insert(0, info)

    if not debug:
        try:
            os.remove(MOVIE_FILE)
            os.remove(TMP_FILE)
        except Exception as e:
            logger.log('unable to remove files', str(e))
    else:
        print('debug on', TMP_FILE)

    return {'name': fn_name, 'exec': exec_info, 'memory': memory_info, 'code': code}

def trace_function(fn, params=[], context=[], debug=False):

    if not callable(fn):
        raise AttributeError(fn + ' must be a function')

    fn_name = fn.__name__

    import inspect
    # trace assumes (for now) that the function to trace goes first
    # context after
    code = inspect.getsource(fn) + '\n'
    for fn_c in context:
        code += inspect.getsource(fn_c) + '\n'

    try:
        import pysnooper
    except ImportError:
        os.system('pip install pysnooper')

    import pysnooper
    data = trace_code(code, fn_name, params=params, debug=debug)
    return data


def generate_files(data, as_one=True, out_dir='.'):
    import json

    html = build_html(data)

    js_data = json.dumps(data)

    if as_one:
        dir_name = os.path.dirname(__file__)
        filename = "{:s}/movie.js".format(dir_name)
        with open(filename, 'r') as fd:
            js_code = fd.read()
            js_out = 'var data = ' + js_data + ';\n' + js_code

        filename = "{:s}/magic.css".format(dir_name)
        with open(filename, 'r') as fd:
            css_code = fd.read()

        doc = '<html><head>'
        script = '<script>{:s}</script>'.format(js_out)
        doc += '<style>{:s}</style>'.format(css_code)
        doc += '</head><body class="lesson">{:s}{:s}</body></html>'.format(html, script)
        return doc

    i_dir = sandbox.get_sandbox_dir()
    code_json = "{:s}/code.json".format(i_dir)
    code_html = "{:s}/code.html".format(i_dir)
    with open(code_html, 'w') as fd:
       jsx = html.replace('class', 'className')
       fd.write(jsx)
    with open(code_json, 'w') as fd:
        fd.write(js_data)
    return 'code.html, code.json'

def play_movie(fn, params=[], context=[], debug=False):

    # MagicTrace.play_movie(all_even, cell_code=In[-1])

    try:
        global In
        c = In[-1]
        logger.log('Found it')
        # need to clean it up to keep only functions
        # could be recursive mess otherwise
    except NameError:
        pass
        #logger.log('In is out')

    data = trace_function(fn, params=params, context=context, debug=debug)
    raw_html = generate_files(data, as_one=True)

    try:
        from IPython.display import display, clear_output
        import IPython
        display(IPython.display.HTML(raw_html))
    except ImportError:
        return raw_html

