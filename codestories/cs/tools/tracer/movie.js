let player = function(data) {
    let stateCheck = setInterval(function(){
        if (document.readyState === 'complete') {
            clearInterval(stateCheck);
            let exec = data['exec']
            let count = 0
            for (let i = 0; i < exec.length; i++) {
                let e_type = exec[i]["type"]
                let e_value = exec[i]["value"]
                let n_value = e_value['name']
                let v_value = e_value['value']

                if (e_type === 'run'){
                    count += 1
                    let lno = v_value
                    let key = 'lno_' + lno;
                    let s1 = document.getElementById(key);
                    if (s1) {
                        const code = {
                            start() {
                                // console.log('adding', this)
                                this.classList.toggle('active')
                            },
                            finish() {
                                this.classList.toggle('active')
                            }
                        }
                        let child = s1.getElementsByClassName('tcontent')[0]
                        code.child = child;

                        // millisecond
                        let time = 1000.0
                        setTimeout(code.start.bind(child),count*time);
                        setTimeout(code.finish.bind(child),(count+1)*time);

                    }
                }
                else if (e_type === 'memory' || e_type === 'output'){
                    let name = n_value
                    let value = v_value
                    let key = 'var_' + name;
                    let s1 = document.getElementById(key);
                    if (s1) {
                        let child = s1.getElementsByClassName('value')[0]
                        const code = {
                            update() {
                                this.node.innerHTML = this.value
                                this.parent.classList.toggle('active')
                            },
                            finish() {
                                this.parent.classList.toggle('active')
                            }
                        }
                        code.value = value
                        code.parent = s1
                        code.node = child

                        let time = 1000.0
                        setTimeout(code.update.bind(code), count * time);
                        setTimeout(code.finish.bind(code), (count + 1) * time);
                    }
                    else {
                        console.log('unable to find', key)
                    }
                }
            }
            console.log('doc is ready');
        }
    }, 200);
}
player(data);