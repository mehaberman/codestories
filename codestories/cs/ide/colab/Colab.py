
import json

import urllib.parse
import urllib.request
import requests

from ..Notebook import StoryNotebook

# from ..Frameworks import NoOpFramework
# from ..Frameworks import JSON_FILE, ASSET_DIR
from codestories.cs.ide.Frameworks import FrameworkAPI, JSON_FILE, PYTHON_FILE, TMP_PYTHON_FILE
from codestories.cs.ide.Frameworks import TestAPI
from codestories.cs.CodeStories import sandbox
from codestories.cs.parsers.CodeCleaners import CodeCleaner
import codestories.cs.tools.Utilities as Tools
logger = sandbox.get_logger()

# see https://colab.research.google.com/drive/13FCrP4rMDk5fteJOCA0FPyVIxZR8mJKU#scrollTo=2D0H5Djcho_j

class ColabFramework(FrameworkAPI):

    def pre_install(self, meta, client):
        logger.log('pre install')
        installer = ColabNotebookInstaller(notebook_id=meta.notebook_id, notebook_file=meta.notebook_file)
        self.tester = ColabTester(client=client, installer=installer)
        self.installer = installer

    def get_tester(self):
        return self.tester

    def post_install(self, meta):
        # initially notebook_id is the github URL
        # once the user copies the notebook to the drive,
        # then the notebook_id will be updated to the part of the driveID
        import urllib.parse
        # name was url encoded (e.g %20 )
        name = urllib.parse.unquote_plus(self.installer.notebook_name)
        meta.update(notebook_id=self.installer.notebook_id, notebook_name=name)
        meta.update(c_id=self.installer.notebook_id)


class ColabTester(TestAPI):

    def __init__(self, client, installer):

        super().__init__(client=client, meta=sandbox.get_meta())

        self.installer = installer
        self.cleaner = CodeCleaner()

    def _generate_ipynb_file(self, file_out):
        return self.installer.read_ipynb(file_out=file_out)

    def _generate_code_file(self, options=None):

        ipynb = self.installer.read_ipynb(file_out=JSON_FILE)

        meta = sandbox.get_meta()
        nb = StoryNotebook(ipynb)
        meta.update(min_time=nb.min_time, max_time=nb.max_time, user=nb.user)

        try:
            code = self.cleaner.clean(nb, options=options)

            # POSSIBLE:
            # use extractor to make into a module
            #
            # pros:  if there's an error, you can look at the solution.py file to match lines
            #        do an import to catch errors too before sending to server
            #
            # cons:  better errors if student imports an unsupported libraray
            #

            with open(PYTHON_FILE, 'w') as fd:
                fd.write(code)
            return PYTHON_FILE
        except Exception as e:
            msg = str(e)
            logger.log('Error on parse', msg)
            print('code was unable to be parsed:')
            print(msg)

            import re
            search = re.search(r'line\s+([0-9]+)', msg)
            match_num = -1
            if search:
                match_num = int(search.group(1))
            else:
                print('ask for help')

            line_no = 0
            with open(TMP_PYTHON_FILE, 'w') as fd:
                for cells in nb.code_cells:
                    for s in cells:
                        s = s.rstrip() + '\n'
                        fd.write(s)
                        line_no += 1
                        if line_no == match_num:
                            print('line with error:', s.rstrip())
            return None


    def is_valid(self, meta):
        if self.installer.validate_notebook_id(meta.notebook_id) is None:
            msg = "Invalid Notebook; Please copy to google drive"
            return False, msg
        if self.installer.validate_user(meta.user_id) is None:
            # TODO: get NET_ID from config
            msg = "Please update NET_ID, and re-run the first code cell"
            return False, msg
        return True, ''

    def __repr__(self):
        return str('Colab Tester ')


class ColabNotebookInstaller(object):

    def __init__(self, notebook_id=None, notebook_file=None):

        # if the content is static, not much to do
        self.ipynb = None
        if notebook_file is not None:
            print('Colab using static content file')
            with open(notebook_file, 'r') as fd:
                self.ipynb = fd.read()

            self.notebook_id = 'na'
            self.notebook_name = 'na'
            return

        # fetch the notebook
        ERR_MSG = "Please set NOTEBOOK_ID, share notebook, and re-install IDE"

        nb_id = None
        notebook_name = 'na'
        try:
            # always try to read vis messaging
            notebook_name, nb_id = self._get_notebook_id()
        except Exception as e:
            logger.log('fail _get_notebook_id', str(e))

        if nb_id is None and notebook_id is None:
            # we can't do anything else
            raise IOError(ERR_MSG)

        if nb_id is not None and notebook_id is not None:
            # attempt to read someone else's notebook
            if nb_id.strip() != notebook_id.strip():
                raise IOError('Invalid attempt to force read')

        if nb_id is None and notebook_id is not None:
            nb_id = notebook_id

        self.notebook_id    = nb_id
        self.notebook_name = notebook_name

        # https://colab.research.google.com/drive/1enGn7x1GOFhoEM_eCCOVaq8yNFbobRtb#scrollTo=g3JAER86thYg
        # 'google.com/drive/1uVeSsdyN8pPVE5pEjY89OLasPHAqqTzD#scroll'
        # '1uVeSsdyN8pPVE5pEjY89OLasPHAqqTzD'

        nb_id = self.validate_notebook_id(self.notebook_id)
        if nb_id is None:
            logger.log('Unknown ID', self.notebook_id)
            print("🛑 You need to ♻️ Copy to Drive")
        else:
            logger.log('Found ID', nb_id)

        logger.log('colab notebook installed')

    def validate_user(self, user_id):
        if user_id is not None and user_id.strip() != 'CHANGE_ME':
            return user_id
        return None

    def validate_notebook_id(self, notebook_id):
        import re
        regex = re.compile(r'/?[A-Za-z0-9_-]{20,}')
        ids = regex.findall(notebook_id)
        if len(ids) == 0:
            return None
        return ids[0].strip('/')

    def read_ipynb(self, file_out=None):

        if self.ipynb is not None:
            print('using static content')
            return self.ipynb

        ipynb = self._read_ipynb()
        if ipynb is None:
            raise IOError('unable to read ipynb file')

        # write it out
        if file_out is not None:
            with open(file_out, 'w') as fd:
                fd.write(ipynb)
        return ipynb

    def __repr__(self):
        return "{:s} {:s}".format(self.notebook_id, self.notebook_name)

    def _read_ipynb(self):
        # try to get it's contents
        ipynb = None
        try:
            # see https://stackoverflow.com/questions/63671205/how-to-retrive-the-notebook-contents-or-the-url-of-a-particular-cell-in-google-c
            # NOTE if notebook_id is someother notebook
            # this will still fetch from the current notebook
            from google.colab import _message

            # Load the notebook JSON.
            msg_js = _message.blocking_request('get_ipynb')  # 'get_ipynb' is request_type
            if msg_js is None:
                logger.log('timeout happened, try no timeout')
                # https://github.com/googlecolab/colabtools/blob/main/google/colab/_message.py
                msg_js = _message.blocking_request('get_ipynb', timeout_sec=None)

            if msg_js is not None:
                content = msg_js.get('ipynb', None)
                if content is None:
                    logger.log('unable to find key ipynb using _message', json.dumps(msg_js))
                else:
                    ipynb = json.dumps(content)
            else:
                logger.log('_message.blocking_request returning None', _message)


        except Exception as e:
            logger.log('invalid _message read', str(e))

        if ipynb is None:

            logger.log('installing via http fetch')

            # TODO:
            #
            # tell user to install magic command to do autosave
            #

            ipynb = install_gd_file(self.notebook_id)
            if ipynb is None or not Tools.is_ipython(ipynb):
                ERROR_MSG = "Make notebook viewable or do a Runtime->Factory reset runtime"
                raise IOError(ERROR_MSG)

        return ipynb

    def _get_notebook_id(self):

        from notebook.notebookapp import list_running_servers

        for s in list_running_servers():
            logger.log(s)
            host = s.get('url', None)
            if host is None:
                print('Unknown host', s)
                continue

            host = host.strip()
            if host[-1] == '/':
                host = host[:-1]

            url = "{:s}/api/sessions/".format(host)
            logger.log(url)

            r = requests.get(url)
            r.encoding = 'utf-8'
            d = r.json()[0]
            # { 'kernel': {'connections': 1, 'last_activity': '2021-09-02T01:15:20.365218Z', 'execution_state': 'busy', 'id': '910b82be-4fad-48ed-b7b9-a5bb0b8ef346', 'name': 'python3'},
            #   'name': 'Functions',
            #   'notebook': {'path': 'fileId=https%3A%2F%2Fgithub.com%2FhabermanUIUC%2FCodeStories-lessons%2Fblob%2Fmain%2Flessons%2Fp4ds%2Fbootcamp%2Ffunctions%2Ffunctions.ipynb', 'name': 'Functions'},
            #   'path': 'fileId=https%3A%2F%2Fgithub.com%2FhabermanUIUC%2FCodeStories-lessons%2Fblob%2Fmain%2Flessons%2Fp4ds%2Fbootcamp%2Ffunctions%2Ffunctions.ipynb',
            #   'type': 'notebook',
            #   'id': '54e6b4f7-ab71-4b67-9fe9-41ee6e7c6e8b'}

            file_id = d['path'].split('=')[1]
            if file_id.startswith('http'):
                print("You need to copy the notebook to your drive")
                file_id = 'did_not_copy'
            return d['name'], file_id
        return None, None




def install_gd_file(doc_id):

    #
    # possible 403 if attempt is made too many times to download?
    # seems to be temporary -- don't fire off too many requests
    #
    baseurl = "https://docs.google.com/uc"
    baseurl = "https://drive.google.com/uc"
    #
    # can help by switching the baseurl
    #

    params = {"export": "download", "id": doc_id}
    url = baseurl + "?" + urllib.parse.urlencode(params)


    def v1():
        logger.log('fetching google doc', url)
        r = urllib.request.urlopen(url)
        status = r.getcode()
        if status != 200:
            print(r.status_code, "unable to download google doc with id:", doc_id)
            return None
        return str(r.read().decode('UTF-8'))

    def v2():
        #r = requests.get(baseurl, params)
        logger.log('fetching google doc', url)
        r = requests.get(url)
        r.encoding = 'utf-8'
        if r.status_code != 200:
            logger.log('bad request:', r.status_code)
            logger.log('headers:', r.headers)
            print(r.status_code, "unable to download google doc with id:", doc_id)
            return None
        return r.text

    try:
        return v2()
    except Exception as e:
        logger.log('fetch', str(e))
        return None