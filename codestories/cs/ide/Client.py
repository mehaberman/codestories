
import os
import json
import requests

import codestories.cs.ide.Utils as ZipLib
from codestories.cs.CodeStories import sandbox

VERSION = '01.01.2021'

SERVER = 'http://localhost:8080'
SERVER = 'http://75.156.71.78:8080'     # MEH
SERVER = 'http://18.219.123.225:8080'   # AWS v1
SERVER = 'http://13.59.43.215:8080'     # AWS v2

SERVER = 'http://18.222.172.177:8080'   # AWS v3

SERVER = 'http://174.136.110.124:8080'  # neo 3gne

# 08.30.2020 arpnetworks issue, switched to AWS
# 08.31.2020 switched back to arpnetworks


class ClientAPI(object):

    def __init__(self, server_id=None):

        if server_id is None:
            server_id = SERVER
        else:
            print('using custom server')

        self.server = server_id
        self.logger = sandbox.get_logger()

        self.logger.log("client version:", VERSION)
        self.logger.log("server:", server_id)

    def _register_session(self):

        did_create, m_time = sandbox.get_session_information()
        if not did_create:
            self.logger.log('skip register')
            return

        self.logger.log("session created", m_time)

        meta = sandbox.get_meta()
        end_point = "{:s}/register".format(self.server)
        post_data = {"lesson_tag": meta.lesson_tag,
                     "c_id": meta.c_id,
                     "u_id": meta.user_id,
                     "u_name": meta.u_name,
                     "mount_time": m_time}

        response = requests.post(end_point, data=post_data)
        if response.status_code != 200:
            self.logger.log('session did not register', response.status_code)
        else:
            self.logger.log('register session at', m_time)

        return None

    def get_tests(self):

        # some times this will be the first request
        self._register_session()

        meta = sandbox.get_meta()
        end_point = "{:s}/list".format(self.server)
        post_data = {"lesson_tag": meta.lesson_tag,
                     "c_id": meta.c_id,
                     "u_id": meta.user_id,
                     "u_name": meta.u_name}

        response = requests.post(end_point, data=post_data)
        error = None
        data = None
        if response.status_code != 200:
            error = str(response.status_code)
            self.logger.log('unable to list', error)
        else:
            r_data = response.json()  # json.loads(r.text)
            error = r_data['error_code']
            if error is None:
                data = r_data['payload']

        return error, data

    def send_zip(self, zipfile, extra_kv={}):

        try:
            self._register_session()
        except Exception as e:
            #except (ConnectionError, ConnectionRefusedError) as e:
            self.logger.log('ERROR (is server running?)', str(e))
            msg = {'error_code': 'Unable to connect to server'}
            return msg

        meta = sandbox.get_meta()
        end_point = "{:s}/testzip".format(self.server)
        # add in the meta data (notebook, assignment, etc)
        post_data = extra_kv
        kv = meta.kv()
        for k in kv:
            v = kv[k]
            # self.logger.log('adding', k, v, ' TO ', post_data)
            if isinstance(v, dict):
                v = json.dumps(kv[k])
            post_data[k] = v

        self.logger.log('sending to test', post_data)
        filename = os.path.basename(zipfile)
        with open(zipfile, 'rb') as fd:
            response = requests.post(end_point, data=post_data,
                                     files={"archive": (filename, fd)})

            data = {'error_code': None, 'payload': {}}
            if response.status_code == 200:
                data = response.json()  # json.loads(r.text)
            else:
                data['error_code'] = response.status_code

            return data
    #
    # all tests must return TWO values (so NoOp will always work)
    #
    def test_file(self, filename, fn_name=None, syntax_only=False):

        if filename is None:
            return "invalid python file/code", None

        extra_kv = {"syntax_only": syntax_only,
                    "fn": fn_name}

        zip_file = ZipLib.create_zipfile(filename, sandbox.get_sandbox_dir())
        # print('ZIP', zip_file, os.getcwd())

        response = self.send_zip(zip_file, extra_kv=extra_kv)
        self.logger.log(response)

        error = response['error_code']
        if error is None:
            payload = response['payload']
            result  = payload['test_result']

            if result['score'] != 100:
                # this gives some info back
                self.logger.log(json.dumps(result))
            return None, result
        else:
            return error, None

    def test_function(self, filename, fn_name):
        error, result = self.test_file(filename, fn_name)
        if error is None:
            message = "0:0:No tests for '{:s}'".format(fn_name)

            # it's possible that the test matched a few possibilities
            # so search for the exact match

            # a test can have a name or a description (via docstring)
            # a test can be called using the test name (function name)
            # but if the docstring is using a verbose string, it won't match
            # the unittest uses the docstring as the name of the test

            only_one = len(result['tests']) == 1
            for t in result['tests']:
                if t['name'] == fn_name or only_one:
                    score = t.get('score', 0)
                    max_score = t.get('max_score', 100)
                    output = t.get('output', "")
                    if score == max_score:
                        # hints will have 0 score, 0 max_score
                        if score != 0:
                            output = "Passed: " + output
                    else:
                        output = "Incomplete: " + output
                    message = "{}:{}:{}".format(score, max_score, output.strip())
                    break
            return None, message
        else:
            return error, "0:0:NA"

    def test_functionality(self, filename, fn_name):
        return self.test_function(filename, fn_name)

