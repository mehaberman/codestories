
import os
import sys

import codestories.cs.SandBox as SandBox

JSON_FILE = '{:s}/solution.json'.format(SandBox.SANDBOX_DIR)
PYTHON_FILE = '{:s}/solution.py'.format(SandBox.SANDBOX_DIR)
TMP_PYTHON_FILE = '{:s}/tmp.py'.format(SandBox.SANDBOX_DIR)

# location of reading assets
ASSET_DIR = "{:s}/{:s}".format(SandBox.SANDBOX_DIR, "INFO490Assets")

# these may need the above constants
from codestories.cs.ide.Client import ClientAPI
from codestories.cs.ide.Notebook import StoryNotebook

class No_Op(object):
    def __init__(self, e):
        self.e = e

    def nop(self, *args, **kw):
        return ("unable to test:" + self.e, None)

    def __getattr__(self, _):
        return self.nop

class FrameworkAPI(object):

    def pre_install(self, meta, client):
        raise NotImplementedError

    def post_install(self, meta):
        pass

    # returns the TestAPI instance
    def get_tester(self):
        raise NotImplementedError



class StaticFileAPI(FrameworkAPI):

    def pre_install(self, meta, client):

        print('preinstall static', meta)
        self.client = client
        self.meta = meta
        try:
            self.filename = meta.notebook_file
        except AttributeError as e:
            print('no filename given, errors on read')

    def get_tester(self):
        return self

    # TEST API
    def _generate_ipynb_file(self, file_out):
        with open(self.filename, 'r') as fd:
            return fd.read()

    def _generate_code_file(self, options=None):
        print("TODO: convert ipynb to python")
        raise NotImplementedError

    def _debug(self):
        return str(self.meta)


class DefaultFramework(object):

    def __init__(self, meta, server_id=None):

        self.client = ClientAPI(server_id=server_id)
        self.logger = SandBox.SandBox().get_logger()

        no_op = No_Op('Base Framework')

        parts = meta.lesson_tag.split(':')
        self.course    = parts[0] # dmap
        self.classroom = parts[1] # projects (ignored for now)
        self.lesson    = parts[2] # project-m3

        self.ASSET_SRC = "{:s}/{:s}/{:s}/{:s}".format(ASSET_DIR, self.course, self.classroom, self.lesson)

    def install_api(self, meta, frameworkAPI):

        frameworkAPI.pre_install(meta, self.client)
        self.api = frameworkAPI.get_tester()

        ipynb = self.api.generate_ipynb_file(file_out=JSON_FILE)
        nb = StoryNotebook(ipynb)

        meta.update(lesson_name=nb.name)
        meta.update(root=nb.root)
        meta.update(raw=nb.get_raw_metadata())

        if nb.tag is None:
            print('Warning: notebook has no tag')
        elif nb.tag != meta.lesson_tag:
            self.logger.log(nb.tag, meta.lesson_tag)
            raise IOError('invalid lesson tag')

        self.install(meta)

        frameworkAPI.post_install(meta)

    def install(self, meta):

        # lesson root
        lesson_root = meta.root

        # "root": "https://raw.githubusercontent.com/habermanUIUC/CodeStories-lessons/main/lessons/p4ds/bootcamp/welcome"

        # root = "{:s}/{:s}/{:s}/{:s}".format(git_root, self.course, self.classroom, self.lesson)

        '''
        old way
         "root": "https://github.com/NSF-EC/INFO490Assets.git",
         "tag": "dmap:projects:project-m3"

         git clone "https://github.com/NSF-EC/INFO490Assets.git" assets
         os.add path(assets/src/dmap/projects/projects-m3)
        '''

        def version_gzip():

            # install the asset directory
            if not os.path.exists(ASSET_DIR):
                os.mkdir(ASSET_DIR)

            url = '{:s}/{:s}.tar.gz?raw=True'.format(lesson_root, self.lesson)
            cmd = "curl -s -L {:s} | tar -zxv -C {:s} &> {:s}/install.log".format(url, ASSET_DIR, ASSET_DIR)
            self.logger.log(cmd)
            ex = os.system(cmd)
            if ex != 0:
                print("Unable to install assets, seek help")
                print('exit code', ex, ex == 0)

            # this returns False -- perhaps the os needs to settle
            fq = os.path.abspath(self.ASSET_SRC)
            if not os.path.exists(fq):
                self.logger.log('path exists?:', fq, os.path.exists(fq))

        def version_git():
            # assert git_root is not None, 'Invalid notebook format'
            # once git 2.19 is installed on colab, we can export/clone a subdirectory
            #
            if git_root is None:
                self.logger.log('unable to find asset root')
                git_root = "https://github.com/NSF-EC/INFO490Assets.git"
            if not os.path.exists(ASSET_DIR):
                self.logger.log("cloning git repos", git_root, ASSET_DIR)
                cmd = "git clone {:s} {:s}".format(git_root, ASSET_DIR)
                os.system(cmd)
            else:
                self.logger.log("reloading git repos", ASSET_DIR)
                cmd = "cd {:s}; git pull".format(ASSET_DIR)
                os.system(cmd)

        def version_svn():
            pass
            # requires subversion to be installed
            # !apt install subversion >& install.log
            # URL = 'https://github.com/NSF-EC/INFO490Assets/trunk/src/dmap/lessons/baseball'
            # from svn import remote
            # r = remote.RemoteClient(URL)
            # r.export('./content/lesson')
            #!svn export 'https://github.com/NSF-EC/INFO490Assets/trunk/src/dmap/lessons/baseball'

        version_gzip()

        if self.ASSET_SRC not in sys.path:
            sys.path.append(self.ASSET_SRC)

    def __repr__(self):
        return 'DefaultFramework'

    def get_tester(self):
        return self.api


from datetime import datetime
import codestories.cs.tools.Utilities as Tools

class TestAPI(object):

    def __init__(self, meta, client):

        self.client = client
        self.meta = meta
        self.logger = SandBox.SandBox().get_logger()
        self.options = None

    def generate_ipynb_file(self, file_out):
        return self._generate_ipynb_file(file_out)

    def generate_code_file(self, options=None):

        # notebook has to be in a good state
        ok, msg = self.is_valid(self.meta)
        if not ok:
            print(msg)
            return None

        if self.options is None:
            # ipynb = self._generate_ipynb_file(JSON_FILE)
            # nb = StoryNotebook(ipynb)
            # self.options = nb.get_raw_metadata().get('parser', {})
            self.options = self.meta.raw.get('parser', {})

        if options is None:
            options = self.options

        return self._generate_code_file(options=options)

    #
    # subclasses
    #

    # return False, if the notebook should not be tested (bad configuation)
    def is_valid(self, meta):
        msg = 'is_valid is not implemented'
        return False, msg

    def _generate_ipynb_file(self, file_out):
        raise NotImplementedError

    def _generate_code_file(self, options=None):
        raise NotImplementedError

    def _debug(self):
        return str(self.meta)


    #
    # PUBLIC API
    #

    def hello_world(self):

        min_ts = self.meta.min_time
        max_ts = self.meta.max_time

        min_dt = datetime.fromtimestamp(min_ts)
        max_dt = datetime.fromtimestamp(max_ts)

        hrs = (max_dt - min_dt).total_seconds()/(60.0*60.0)
        min_s = Tools.timestamp_to_str(min_ts)
        max_s = Tools.timestamp_to_str(max_ts)

        print("Hello\n{:s}\n{:s} ({:f})".format(min_s, max_s, hrs))

    def is_notebook_valid_python(self):
        filename = self.generate_code_file(options={'as_is': 'true'})
        e, r = self.client.test_file(filename, syntax_only=True)
        return e is None, e

    def clean_notebook_for_download(self, rename=False):
        # remove magic cells
        # will remove the entire download_notebook cell
        # since it using google.files (see parser)
        filename = self.generate_code_file()
        e, r = self.client.test_file(filename, syntax_only=True)
        if e is None:
            if rename:
                #
                # rename solution.py to <lesson>.py
                #
                import shutil
                new_name = "{:s}.py".format(self.meta.lesson)
                shutil.copyfile(filename, new_name)
                filename = new_name
            return True, filename
        else:
            return False, e

    def list_tests(self):
        e, r = self.client.get_tests()
        if e is None:
            print(r)
        else:
            print('Error', e)
        return None

    def test_notebook(self, verbose=False, max_score=100):

        filename = self.generate_code_file()
        e, r = self.client.test_file(filename)

        if e is not None:
            return "ERROR: {:s}".format(str(e))

        score = int(float(r.get('score', 0)))
        score_msg = "Score {:d}/{:d}\n".format(score, max_score)
        if not verbose:
            return score_msg

        # verbose output
        buffer = [score_msg]
        for t in r.get('tests', []):
            name = t.get('name', 'n/a')
            score = t.get('score', 0)
            max_score = t.get('max_score', 0)
            output = t.get('output', '').strip()
            buffer.append("{} {}/{}\n{}".format(name, score, max_score, output))

        return "\n".join(buffer)

    def create_warning(self, what):
        # warning = "If you change " + what + ", SAVE the notebook (⌘/Ctrl s) before retesting"
        warning = "Before re-testing, SAVE (⌘/Ctrl s) any changes."
        return warning

    def test_function(self, fn, verbose=True):

        assert fn is not None, "parameter is None"
        what = "your code"
        if callable(fn):
            fn = fn.__name__
            what = fn

        filename = self.generate_code_file()
        error, msg = self.client.test_function(filename, fn)

        if verbose:
            # if it's verbose, just return a single string
            # to make for easy printing
            warning = self.create_warning(what)
            if error is not None:
                return "Tested: {:s}\nError: {:s}\n{:s}".format(fn, str(error), warning)
            else:
                score, max_score, msg = msg.split(':', maxsplit=2)
                try:
                    score = int(float(score))         # could be 0.0 (gradescope default)
                    max_score = int(float(max_score))
                    if score == max_score:
                        warning = ''
                except Exception as e:
                    self.logger.log('ERROR on verbose', e)

                return "Tested: {:s}\nScore: {:d}\nMax Score: {:d}\nOutput: {:s}\n{:s}".format(fn, score, max_score, msg, warning)

        return error, msg

    def test_with_ui(self, fn):

        if callable(fn):
            fn = fn.__name__

        try:
            import ipywidgets as widgets
            from IPython.display import display, clear_output

            space = ' '  # this is a NO-BREAK-SPACE character
            button = widgets.Button(description="▶️ " + space + fn)
            button.style = widgets.ButtonStyle(button_color='#FFDEAD')
            output = widgets.Output()

            def on_button_clicked(input):
                button.description = '🔄 ...testing'
                error, msg = self.test_function(fn, verbose=False)
                # print("Button clicked.", fn, input)
                # Display the message within the output widget.
                with output:
                    clear_output()  # also removes the button if put before output
                    print_warning = True

                    if error is None:
                        score, max_score, msg = msg.split(':', maxsplit=2)
                        score = int(float(score))  # floats (e.g 0.0) are possible
                        max_score = int(float(max_score))

                        if msg.find('no tests') >= 0:
                            button.style = widgets.ButtonStyle(button_color='yellow')
                            button.description = 'No Tests'
                        elif score > 0 and score == max_score:
                            button.style = widgets.ButtonStyle(button_color='green')
                            button.description = 'Pass!'
                            print_warning = False
                        elif score > 0:
                            button.style = widgets.ButtonStyle(button_color='yellow')
                            button.description = 'More Work'
                        else:
                            button.style = widgets.ButtonStyle(button_color='#ff9999')
                            button.description = 'Fail'
                    else:
                        msg = ''
                        button.style = widgets.ButtonStyle(button_color='#ff9999')  # was red
                        button.description = 'FAIL: {}'.format(fn)
                        print("Error:", error)

                    if print_warning:
                        print(msg)
                        print(self.create_warning(fn))

                    # since the button text changes
                    # disabling the button, makes sense
                    # otherwise, it's not clear that the user COULD press it again
                    # this way it forces the user to reload the cell
                    button.disabled = True

            button.on_click(on_button_clicked)
            display(button, output)

        except ImportError as e:
            return 'unable to test with gui: ', str(e)

    def download_solution(self, rename=True):
        valid, file_or_error = self.clean_notebook_for_download(rename)
        if valid:
            print("{:s} contains valid python; it will be downloaded".format(file_or_error))
            try:
                from google.colab import files
                files.download(file_or_error)
            except ImportError:
                # server side
                pass
            except Exception as e:
                print("{:s}\nError on download: {:s}".format(file_or_error, e))
        else:
            # print("The code contains invalid python")
            print("FIX the following Errors:\n{:s}\n".format(file_or_error))
            print("You can tag questions on Piazza with", "'{}'".format(self.meta.lesson))

    def test_functionality(self, fn, verbose=True):

        # add get_hint(self, name)
        # only show output
        # or if fn.find('hint') ...

        return self.test_function(fn, verbose=verbose)


